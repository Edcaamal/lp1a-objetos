/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos1;

/**
 *
 * @author edgar
 */
public class test {
    public static int x;
    
    public static int cuadrado(int lado){
        return lado*lado;
    }

    public static void main(String[] args) {
         test.x = 10;
         System.out.println(test.x);
         System.out.println(cuadrado(5));
         System.out.println(test.cuadrado(6));
         
        // Primera forma para crear y llamar a metodos de un clase
        cuadrado figura = new cuadrado();
        figura.setLado(6);
        System.out.println(figura.area());
        
        // Segunda forma para crear y llamar a metodos de un clase
        cuadrado cuatrolados = new cuadrado(7);
        System.out.println(cuatrolados.area());
        
        electrodomestico lavadora = new electrodomestico();
        lavadora.setMarca("Mabe");
        lavadora.setModelo("Centrifugado");
        lavadora.setNumeroSerie("12345ABC");
        lavadora.setCapacidad(12);
        
        System.out.println(lavadora.encender());
        
        System.out.println(lavadora.toString());
        System.out.println(lavadora.apagar());
        
        electrodomestico refrigerador = new electrodomestico("GE", "Sin escarcha", "ABC12345", 14);
        System.out.println(refrigerador.toString());
        
        System.out.println(refrigerador.getNumeroSerie());
        refrigerador.setMarca("Samsumg");
        System.out.println(refrigerador.toString());
        
        personas al001 = new personas("Alumno", "Juan Manuel", 'M', 22, 1, 1);
        personas al002 = new personas("Alumno", "Perla", 'F', 21, 1, 2);
        
        personas ma001 = new personas("Maestro", "Manuel Esteban", 'M', 35, 2, 3);
        personas ad001 = new personas("Administrativo", "Juan Jose", 'M', 40, 5, 4);
        System.out.println(al001.toString());
        System.out.println(al002.toString());
        System.out.println(ma001.toString());
        System.out.println(ad001.toString());
      
    }    
}
