/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos1;

/**
 *
 * @author edgar
 */
public class electrodomestico {
    private String marca;
    private String modelo;
    private String numeroSerie;
    private int capacidad;
    
    // Constructor vacio
    public electrodomestico() {
    }
    // Constructor y asignación de valores
    public electrodomestico(String marca, String modelo, String numeroSerie, int capacidad) {
        this.marca = marca;
        this.modelo = modelo;
        this.numeroSerie = numeroSerie;
        this.capacidad = capacidad;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String encender (){
        return "Electrodomestico Encendido ...";
    }    

    public String apagar (){
        return "Apagando Electrodomestico ...";
    }  

    @Override
    public String toString() {
        return "electrodomestico{" + "marca=" + marca + ", modelo=" + modelo + ", numeroSerie=" + numeroSerie + ", capacidad=" + capacidad + '}';
    }
    

    
    
    
}
