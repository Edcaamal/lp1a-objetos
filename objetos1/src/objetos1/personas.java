/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos1;

/**
 *
 * @author edgar
 */
public class personas {
    private String tipo;
    private String nombre;
    private char sexo;
    private int edad;
    private int antiguedad;
    private int id;

    public personas() {
    }

    public personas(String tipo, String nombre, char sexo, int edad, int antiguedad, int id) {
        this.tipo = tipo;
        this.nombre = nombre;
        this.sexo = sexo;
        this.edad = edad;
        this.antiguedad = antiguedad;
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "personas{" + "tipo=" + tipo + ", nombre=" + nombre + ", sexo=" + sexo + ", edad=" + edad + ", antiguedad=" + antiguedad + ", id=" + id + '}';
    }
    
    
    
    
}
