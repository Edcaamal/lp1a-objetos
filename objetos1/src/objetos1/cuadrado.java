/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos1;

/**
 *
 * @author edgar
 */
public class cuadrado {
    private int lado;

    public cuadrado() {
    }
    
    public cuadrado(int lado) {
        this.lado = lado;
    }


    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }
    
    public int area (){
        return lado * lado;
    }

    @Override
    public String toString() {
        return "cuadrado{" + "lado=" + lado + '}';
    }
    
    
}
